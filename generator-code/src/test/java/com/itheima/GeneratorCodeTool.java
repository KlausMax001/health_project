package com.itheima;

import com.itheima.core.GenerateCode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class GeneratorCodeTool {

    @Resource
    private GenerateCode generateCode;

    @Test
    public void generator(){
        generateCode.generator();
    }

}
