select
===
SELECT
    TABLE_NAME table_name,
    column_name column_name,
    DATA_TYPE data_type,
    column_comment comments,
    (case when COLUMN_KEY = 'PRI' then 1 else 0 end) iskey
FROM
    information_schema.COLUMNS
where
    TABLE_SCHEMA = #catalog#
    and TABLE_NAME = #tableName#
    and column_name <> 'last_update_time'
