package com.itheima.dto;

import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import lombok.Data;

import java.util.List;

@Data
public class CheckGroupDto extends CheckGroup {

    private List<CheckItem> checkItems;

}
