package com.itheima.dto;

import com.itheima.pojo.Setmeal;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<CheckGroupDto> checkGroups;

}
