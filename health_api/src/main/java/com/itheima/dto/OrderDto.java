package com.itheima.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderDto implements Serializable {

    private String member;//体检人
    private String setmeal;//套餐
    private String orderDate;//预约日期
    private String orderType;//预约类型
    private String orderStatus;//订单状态

}
