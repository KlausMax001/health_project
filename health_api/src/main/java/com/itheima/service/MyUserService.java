package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.MyUser;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/21 20:47
 */
public interface MyUserService extends IService<MyUser> {
}
