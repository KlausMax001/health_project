package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.CheckItem;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/15 17:51
 */
public interface CheckItemService extends IService<CheckItem> {

    PageResult findByPage(QueryPageBean queryPageBean);

    PageResult queryByPage(QueryPageBean queryPageBean);

    List<CheckItem> findByGroupId(Long id);
}
