package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.SetmealDto;
import com.itheima.pojo.Setmeal;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.vo.SetMealVO;


public interface SetmealService extends IService<Setmeal> {

    PageResult findByPage(QueryPageBean queryPageBean);

    boolean saveSetMeal(SetMealVO setMealVO);

    /**
     * 清理oss图片文件垃圾碎片的接口方法
     */
    void clearOssPic();

    SetmealDto detail(Long setmealId);
}
