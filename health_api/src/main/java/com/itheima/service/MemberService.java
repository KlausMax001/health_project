package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Member;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/24 19:29
 */
public interface MemberService extends IService<Member> {

    Member findByTelephone(String telephone);
}
