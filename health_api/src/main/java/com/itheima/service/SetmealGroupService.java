package com.itheima.service;

import com.itheima.pojo.SetmealGroup;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SetmealGroupService extends IService<SetmealGroup> {

    Long[] selectGroupBySetMealId(Long setMealId);
}
