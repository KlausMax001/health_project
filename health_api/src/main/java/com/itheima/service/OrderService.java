package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.OrderDto;
import com.itheima.pojo.Order;
import com.itheima.vo.OrderVO;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/22 16:37
 */
public interface OrderService extends IService<Order> {

    Long add(OrderVO orderVO);

    Order exists(Long memberId,String orderDate);

    OrderDto findOrderDtoByOrderId(Long id);
}

