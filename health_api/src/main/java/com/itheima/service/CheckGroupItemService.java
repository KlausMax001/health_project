package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.CheckGroupItem;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:36
 */
public interface CheckGroupItemService extends IService<CheckGroupItem> {
    /**
     * 通过id删除记录
     * @param id
     */
    void deleteByGroupId(Long id);

    /**
     * 批量插入检
     * @param id
     * @param items
     */
    void saveCheckGroupItem(Long id, Long[] items);

    /**
     * 根据检查组id查询对应检查项号
     * @param groupId
     * @return
     */
    Long[] selectItemsByGroupId(Long groupId);
}
