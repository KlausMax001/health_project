package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.CheckGroupDto;
import com.itheima.pojo.CheckGroup;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.vo.CheckGroupVO;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:38
 */
public interface CheckGroupService extends IService<CheckGroup> {

    boolean saveGroup(CheckGroupVO checkGroupVO);

    PageResult findAllByPage(QueryPageBean queryPageBean);

    List<CheckGroupDto> selectBySetmeal(Long setmealId);
}
