package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingService extends IService<OrderSetting> {

    void importData(List<String[]> list);

    Map findOrderSettingByYearMonth(String yearMonth);

    Boolean updateOrderSettingByData(String data, String num);

    OrderSetting findByDate(String orderDate);

}
