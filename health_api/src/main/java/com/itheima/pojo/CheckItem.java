package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/15 9:59
 */
@Data
@TableName("tb_check_item")
public class CheckItem  implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    private String code;//项目编码
    private String name;//项目名称
    private String sex;//性别(0-不限,1-男,2-女)
    private String age;//适用年龄
    private String type;//类型(1-检查,2-检验)
    private BigDecimal price;//价格
    private String remark;//项目说明
    private String attention;//注意事项
    private Integer deleteFlag;//是否删除
}
