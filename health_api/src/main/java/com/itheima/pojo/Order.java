package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_order")
public class Order implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    private Long memberId;//会员ID
    private Long setmealId;//套餐ID
    private Date orderDate;//预约日期
    private String orderType;//预约类型
    private Integer orderStatus = 1;//预约状态(1-预约成功,2-预约到诊)

}
