package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("tb_setmeal")
public class Setmeal implements Serializable {
	/**
	* ID
	*/
    @TableId(type = IdType.AUTO)
	private Long id;

	/**
	* 名称
	*/
	@NotBlank(message = "套餐名称不能为空")
	private String name;

	/**
	* 编码
	*/
	@NotBlank(message = "套餐编码不能为空")
	private String code;

	/**
	* 适用性别
	*/
	private String sex;

	/**
	* 助记码
	*/
	private String helpCode;

	/**
	* 套餐价格
	*/
	private BigDecimal price;

	/**
	* 适用年龄
	*/
	private String age;

	/**
	* 图片
	*/
	private String img;

	/**
	* 说明
	*/
	private String remark;

	/**
	* 注意事项
	*/
	private String attention;

	/**
	* 是否删除(0-未删除,1-已删除)
	*/
	private Integer deleteFlag;


}