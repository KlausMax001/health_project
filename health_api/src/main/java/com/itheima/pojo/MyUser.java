package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName("tb_user")
@Data
public class MyUser implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID主键
    private String userName;//用户名
    private String password;//密码
    private String name;//姓名
    private Integer age;//年龄
    private Integer sex;//性别
    private Date birthday;//出生日期
    private String email;//电子邮箱
    private String authority;//权限

}
