package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_order_setting")
public class OrderSetting implements Serializable {
	/**
	* ID
	*/
    @TableId(type = IdType.AUTO)
	private Long id;

	/**
	* 预约日期
	*/
	private Date orderDate;

	/**
	* 可预约总数
	*/
	private Integer number;

	/**
	* 已预约数
	*/
	private Integer reservations;


}