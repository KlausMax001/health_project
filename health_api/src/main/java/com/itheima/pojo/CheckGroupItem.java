package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 17:56
 */
@Data
@TableName("tb_check_group_item")
public class CheckGroupItem {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    private Long groupId;//
    private Long itemId;//
}
