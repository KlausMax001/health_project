package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_member")
public class Member implements Serializable {
	/**
	* ID
	*/
    @TableId(type = IdType.AUTO)
	private Long id;

	/**
	* 体检人
	*/
	private String name;

	/**
	* 性别
	*/
	private String sex;

	/**
	* 手机号
	*/
	private String telephone;

	/**
	* 身份证号
	*/
	private String idCard;

	/**
	* 创建日期
	*/
	private Date createDate;


}