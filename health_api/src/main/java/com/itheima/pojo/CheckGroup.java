package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 17:55
 */
@Data
@TableName("tb_check_group")
public class CheckGroup implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    @NotBlank(message = "项目编码不能为空")
    private String code;//项目编码
    @NotBlank(message = "项目名称不能为空")
    private String name;//项目名称
    private String sex;//性别(0-不限,1-男,2-女)
    private String helpCode;//助记码
    private String remark;//项目说明
    private String attention;//注意事项
    private Integer deleteFlag;//是否删除

}
