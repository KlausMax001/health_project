package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_setmeal_group")
public class SetmealGroup implements Serializable {
	/**
	* ID
	*/
    @TableId(type = IdType.AUTO)
	private Long id;

	/**
	* 套餐ID
	*/
	private Long setmealId;

	/**
	* 检查组ID
	*/
	private Long groupId;


}