package com.itheima.support;

import lombok.Data;

@Data
public class Result<T> {

    private Integer status = 200;//状态:200-成功,500-错误

    private boolean flag = true;//true=接口调用成功，false=接口调用失败

    private T data;//真实的接口返回数据

    private String errorMsg;//错误描述信息

    public Result(T data){
        this.data = data;
    }

    public Result(boolean flag,String errorMsg){
        this.flag = flag;
        this.errorMsg = errorMsg;
        this.status = 500;
    }

    public static <T> Result<T> build(T data){
        return new Result<>(data);
    }

    public static <T> Result<T> buildError(String errorMsg){
        return new Result<T>(false,errorMsg);
    }

}
