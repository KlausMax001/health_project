package com.itheima.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrderVO implements Serializable {

    private String name;//体检人
    private String sex;//性别
    private String telephone;//手机号
    private String idCard;//身份证号
    private String validateCode;//验证
    private Date orderDate;//预约日期
    private Long setmealId;//套餐ID

}
