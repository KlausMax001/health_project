package com.itheima.vo;

import com.itheima.pojo.Setmeal;
import lombok.Data;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/18 17:16
 */
@Data
public class SetMealVO extends Setmeal {

    private Long[] groups;//勾选的检查组的ID集合
}
