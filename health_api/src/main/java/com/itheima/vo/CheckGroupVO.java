package com.itheima.vo;

import com.itheima.pojo.CheckGroup;
import lombok.Data;

@Data
public class CheckGroupVO extends CheckGroup {

    private Long[] items;//勾选的检查项ID集合

}
