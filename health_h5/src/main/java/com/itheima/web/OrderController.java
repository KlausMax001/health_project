package com.itheima.web;

import com.itheima.dto.OrderDto;
import com.itheima.service.OrderService;
import com.itheima.support.Result;
import com.itheima.vo.OrderVO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/order")
public class OrderController {

    @DubboReference
    private OrderService orderService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @GetMapping("/sendSms/{telephone}")
    public Result<Boolean> sendSms(@PathVariable("telephone") String telephone){
        amqpTemplate.convertAndSend("sms_exchange","sms",telephone);
        return Result.build(true);
    }

    @PostMapping("/add")
    public Result<Long> add(@RequestBody OrderVO orderVO){
        return Result.build(orderService.add(orderVO));
    }

    @GetMapping("/success/{orderId}")
    public Result<OrderDto> success(@PathVariable("orderId")Long Id){
        return Result.build(orderService.findOrderDtoByOrderId(Id));
    }

}
