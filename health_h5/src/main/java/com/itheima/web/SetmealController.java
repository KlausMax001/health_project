package com.itheima.web;

import com.itheima.service.SetmealService;
import com.itheima.support.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @DubboReference
    private SetmealService setmealService;

    @GetMapping("/findAll")
    public Result findAll(){
        return Result.build(setmealService.list());
    }

    @GetMapping("/detail/{setmealId}")
    public Result detail(@PathVariable("setmealId") Long setmealId){
        return Result.build(setmealService.detail(setmealId));
    }

    @GetMapping("/findInfo/{setmealId}")
    public Result findInfo(@PathVariable("setmealId") Long setmealId){
        return Result.build(setmealService.getById(setmealId));
    }

}
