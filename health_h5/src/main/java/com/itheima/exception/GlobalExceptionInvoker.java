package com.itheima.exception;

import com.itheima.support.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionInvoker {

    @ExceptionHandler
    public Result<String> handleException(Exception e){
        String errorMsg = e.getMessage();
        if(errorMsg != null && errorMsg.length() > 250){
            errorMsg = errorMsg.substring(0,250);
        }
        e.printStackTrace();
        return Result.buildError(errorMsg);
    }

}
