package com.itheima.config;

import com.itheima.auth.filter.TokenFilter;
import com.itheima.auth.handle.MyAccessDeniedHandler;
import com.itheima.auth.handle.MyAuthenticationEntryPoint;
import com.itheima.auth.handle.MyAuthenticationFailureHandler;
import com.itheima.auth.handle.MyAuthenticationSuccessHandler;
import com.itheima.utils.MD5Util;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/21 19:07
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 构建密码器
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                //调用密码加密工具类对用户密码加密
                return MD5Util.encode(String.valueOf(rawPassword));
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                System.out.println(rawPassword+"-------"+encodedPassword);

                //将用户密码加密与数据库中传来的加密密码进行匹配
                System.out.println("前端传的密码加密后："+encode(rawPassword));
                System.out.println("数据库中的密码:" + encodedPassword);

                return encode(rawPassword).equals(encodedPassword);

            }
        };
    }

    /**
     *配置web资源放行
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().mvcMatchers(
                "/swagger-ui.html",
                "/webjars/**",
                "/v2/**",
                "/swagger-resources/**");
    }

    /**
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin() //认证相关 /login
                .successHandler(new MyAuthenticationSuccessHandler())//认证成功处理器
                .failureHandler(new MyAuthenticationFailureHandler())//认证失败处理器
                .and()
                .exceptionHandling()// 非登入(/login)异常处理
                .accessDeniedHandler(new MyAccessDeniedHandler())//403无权限拒绝处理器
                .authenticationEntryPoint(new MyAuthenticationEntryPoint())//其它异常处理器
                .and()
                .authorizeRequests()
                .antMatchers("/login*").permitAll()//登入白名单
                .anyRequest().authenticated()//其它地址都需要先登入才可以访问
                .and()
                .csrf().disable() //禁用csrf
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)//禁用session
        ;
        //授权解析token过滤器
        http.addFilterBefore(new TokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
