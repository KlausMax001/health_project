package com.itheima.controller;

import com.itheima.service.CheckGroupItemService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 20:07
 */
@RestController
public class CheckGroupItemController {

    @DubboReference
    private CheckGroupItemService checkGroupItemService;

    @GetMapping("/selectItemsByGroupId/{groupId}")
    public Long[] selectItemsByGroupId(@PathVariable("groupId") Long groupId){
        return checkGroupItemService.selectItemsByGroupId(groupId);
    }


}
