package com.itheima.controller;

import com.itheima.service.OrderSettingService;
import com.itheima.support.Result;
import com.itheima.utils.POIUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @DubboReference
    public OrderSettingService orderSettingService;

    /**
     * 导入excell
     * @param multipartFile
     * @throws IOException
     */
    @PostMapping("/import")
    public void importData(@RequestParam("file")MultipartFile multipartFile) throws IOException {
        List<String[]> list = POIUtils.readExcel(multipartFile.getInputStream(), multipartFile.getOriginalFilename());
        orderSettingService.importData(list);
    }

    @GetMapping("/findOrderSettingByMonth/{yearMonth}")
    public Result<Map> findOrderSettingByYearMonth(@PathVariable("yearMonth")String yearMonth){

        Map map = orderSettingService.findOrderSettingByYearMonth(yearMonth);

        Result<Map> mapResult = Result.build(map);

        return mapResult;
    }

    @GetMapping("/updateOrderSettingByData/{Data}/{num}")
    public Result<Boolean> updateOrderSettingByData(@PathVariable("Data")String data,@PathVariable("num") String num){
        return Result.build(orderSettingService.updateOrderSettingByData(data,num));
    }
}
