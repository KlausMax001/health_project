package com.itheima.controller;

import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.vo.CheckGroupVO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:40
 */
@RestController
@RequestMapping("/checkgroup")
public class CheckGroupController {

    @DubboReference
    private CheckGroupService checkGroupService;


    /**
     * 无分页查询全部检查组信息
     *
     * @return
     */
    @GetMapping("/findAll")
    public List<CheckGroup> findAll() {
        return checkGroupService.list();
    }

    /**
     * 带分页的查询全部检查组信息
     * @return
     */
    @PostMapping("/findAllByPage")
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean) {
        return checkGroupService.findAllByPage(queryPageBean);
    }

    /**
     * 添加检查组
     *
     * @return
     */
    @PostMapping("/save")
    public boolean add(@RequestBody CheckGroupVO checkGroupVO) {
        return checkGroupService.saveGroup(checkGroupVO);
    }

    @GetMapping("/deleteByGroupId/{groupId}")
    public boolean deleteByGroupId(@PathVariable("groupId") Long groupId){

        return checkGroupService.removeById(groupId);
    }




}
