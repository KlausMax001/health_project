package com.itheima.controller;

import com.itheima.utils.FileUtil;
import com.itheima.utils.RedisUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/18 22:28
 */
@RestController
public class UploadController {


    /**
     * 上传图片文件
     * @param multipartFile
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    private String upload(@RequestParam("myfile") MultipartFile multipartFile) throws IOException {
        //调用工具类上传图片文件到阿里云oss服务器上，并返回图片文件名
        String key = FileUtil.upload(multipartFile.getInputStream(), multipartFile.getOriginalFilename());
        //根据设置一个redis set集合 值为文件名key
//        redisTemplate.opsForSet().add("temp-rubbish",key);
        RedisUtil.addToSet("temp-rubbish",key);
        //设置辅助key，针对文件元素，设置过期时间
//        redisTemplate.opsForValue().set("file:"+key,1,2L, TimeUnit.MINUTES);
        RedisUtil.set("file:"+key,1,2L, TimeUnit.MINUTES);

        return key;
    }
}
