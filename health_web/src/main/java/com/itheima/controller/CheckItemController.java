package com.itheima.controller;

import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/15 17:56
 */
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {

    @DubboReference
    private CheckItemService checkItemService;

    @GetMapping("/findAll")
    public List<CheckItem> findAll() {
        return checkItemService.list();
    }

    @PostMapping("/add")
    public boolean add(@RequestBody @Valid CheckItem checkItem) {
        return checkItemService.saveOrUpdate(checkItem);
    }

    /**
     * 分页查询
     *
     * @param queryPageBean
     */
    @PreAuthorize("hasAuthority('admin')")
    @PostMapping("/findbypage")
    public PageResult findByPage(@RequestBody QueryPageBean queryPageBean) {
        return checkItemService.findByPage(queryPageBean);
    }

    /**
     * 条件查询
     * @param queryPageBean
     * @return
     */
    @PostMapping("/queryByPage")
    public PageResult queryByPage(@RequestBody QueryPageBean queryPageBean) {
        return checkItemService.queryByPage(queryPageBean);
    }

    /**
     * 根据id删除检查记录
     * @param id
     * @return
     */
    @GetMapping("/deleteById/{id}")
    public Boolean deleteById(@PathVariable("id") String id) {
        return checkItemService.removeById(id);
    }



}
