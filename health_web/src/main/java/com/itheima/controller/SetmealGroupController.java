package com.itheima.controller;

import com.itheima.service.SetmealGroupService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/18 16:55
 */

@RestController
@RequestMapping("/setmealgroup")
public class SetmealGroupController {

    @DubboReference
    private SetmealGroupService setmealGroupService;

    /**
     * 查询对应套餐下的所有检查组
     * @param setMealId
     * @return
     */
    @GetMapping("/findSetmealById/{id}")
    public Long[] selectGroupsBySetMealId(@PathVariable("id") Long setMealId){
        return setmealGroupService.selectGroupBySetMealId(setMealId);
    }
}
