package com.itheima.controller;

import com.itheima.service.SetmealService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.vo.SetMealVO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/18 17:07
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @DubboReference
    private SetmealService setmealService;

    /**
     * 添加套餐
     * @param setmealVO
     * @return
     */
    @PostMapping("/add")
    public boolean addSetMeal(@RequestBody @Valid SetMealVO setmealVO){
        return setmealService.saveSetMeal(setmealVO);
    }


    /**
     * 分页查询
     *
     * @param queryPageBean
     */
    @PostMapping("/findByPage")
    public PageResult findByPage(@RequestBody QueryPageBean queryPageBean) {
        return setmealService.findByPage(queryPageBean);
    }

    /**
     * 删除指定id的套餐项
     * @param id
     * @return
     */
    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Long id){
        return setmealService.removeById(id);
    }

    /**
     * 调用清理垃圾的方法
     * @return
     */
    @GetMapping("/clearOssPic")
    public boolean clearOssPic(){
       setmealService.clearOssPic();
       return true;
    }
}
