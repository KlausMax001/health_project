package com.itheima.exception;

import com.itheima.support.Result;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/19 19:30
 */
//RequestBody+Controller的增强 全局异常处理类
@RestControllerAdvice
public class GlobalExceptionInvoker {

    //异常处理器

    /**
     * 截取详细异常信息
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result<String> Invoker(Exception ex){
        String msg = ex.getMessage();
        if(ex instanceof MethodArgumentNotValidException){
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
            msg = exception.getBindingResult().getFieldErrors().get(0).getDefaultMessage();
        }else if(ex.getMessage() != null && ex.getMessage().length() > 250){
            msg = ex.getMessage().substring(0,250);
        }
        ex.printStackTrace();

        return Result.buildError(msg);
    }

}
