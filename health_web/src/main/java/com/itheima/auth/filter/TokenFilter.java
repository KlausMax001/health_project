package com.itheima.auth.filter;

import com.itheima.cst.SysConsts;
import com.itheima.pojo.MyUser;
import com.itheima.utils.JwtTokenUtil;
import com.itheima.utils.RedisUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization") == null ? request.getParameter("token") : request.getHeader("Authorization");
        if(!ObjectUtils.isEmpty(token)){
            try {
                String username = JwtTokenUtil.parseToken(token);
                //查询当前登入用户信息
                MyUser user = RedisUtil.get(SysConsts.REDIS_USER_PREFIX + username);
                //身份信息
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        username,
                        null,
                        AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthority())
                );
                //往下传递认证身份信息 本质上是放到了ThreadLocal
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        //放行进入下一过滤链，接着做过滤将
        filterChain.doFilter(request,response);
        }

}
