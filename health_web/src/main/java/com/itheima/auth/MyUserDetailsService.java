package com.itheima.auth;

import com.itheima.cst.SysConsts;
import com.itheima.pojo.MyUser;
import com.itheima.utils.RedisUtil;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/21 19:37
 */
@Component
public class MyUserDetailsService implements UserDetailsService {
    /**
     * 身份校验
     * @param
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        MyUser user = RedisUtil.get(SysConsts.REDIS_USER_PREFIX+username);
        if(user != null){
            return new User(username,user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(user.getAuthority()));
        }
        return null;
    }
}
