package com.itheima.mq;

import com.itheima.utils.RedisUtil;
import com.itheima.utils.SmsUtil;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class SmsConsumer implements InitializingBean {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @RabbitListener(queues = "sms_queue")
    public void recv(String telephone){
        try {
//            String code = SmsUtil.sendSms(telephone);
            String code = SmsUtil.sendSmsCode();
            System.out.println("code:" + code);
            RedisUtil.set("sms:" + telephone,code,5L, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RedisUtil.register(redisTemplate);
    }
}
