package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Member;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/24 19:32
 */
public interface MemberMapper extends BaseMapper<Member> {
}
