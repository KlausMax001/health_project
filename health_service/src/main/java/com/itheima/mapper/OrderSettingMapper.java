package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.OrderSetting;

public interface OrderSettingMapper extends BaseMapper<OrderSetting> {
}
