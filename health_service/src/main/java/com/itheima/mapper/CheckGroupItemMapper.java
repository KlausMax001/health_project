package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.CheckGroupItem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:43
 */
public interface CheckGroupItemMapper extends BaseMapper<CheckGroupItem> {

    @Select("select item_id from tb_check_group_item\n" +
            "where group_id = #{groupId}")
    public Long[] selectItemsByGroupId(@Param("groupId") Long groupId);
}
