package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.SetmealGroup;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface SetmealGroupMapper extends BaseMapper<SetmealGroup> {

    @Select("select group_id from tb_setmeal_group \n" +
            "where setmeal_id = #{setMealId}")
    Long[] selectGroupBySetMealId(@Param("setMealId") Long setMealId);
}
