package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/15 17:52
 */

public interface CheckItemMapper extends BaseMapper<CheckItem> {

    @Select("select i.* from tb_check_group_item g,tb_check_item i\n" +
            "where group_id = #{groupId} and g.item_id = i.id")
    List<CheckItem> findByGroupId(@Param("groupId")Long id);

}
