package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.MyUser;

public interface UserMapper extends BaseMapper<MyUser> {
}
