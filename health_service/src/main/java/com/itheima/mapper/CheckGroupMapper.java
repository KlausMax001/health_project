package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.dto.CheckGroupDto;
import com.itheima.pojo.CheckGroup;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:42
 */
public interface CheckGroupMapper extends BaseMapper<CheckGroup> {
    @Select("SELECT\n" +
            "\tg.*\n" +
            "FROM\n" +
            "\ttb_setmeal_group s,\n" +
            "\ttb_check_group g\n" +
            "WHERE\n" +
            "\tsetmeal_id = #{setmealId} \n" +
            "and g.id = s.group_id;")
    List<CheckGroupDto> selectBySetmeal(@Param("setmealId") Long setmealId);
}
