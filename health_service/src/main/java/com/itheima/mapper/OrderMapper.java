package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.dto.OrderDto;
import com.itheima.pojo.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface OrderMapper extends BaseMapper<Order> {
    @Select("SELECT\n" +
            "\tm.name as member,\n" +
            "\ts.name as setmeal,\n" +
            "  DATE_FORMAT(order_date,\"%Y-%m-%d\") as order_date,\n" +
            "\torder_type,\n" +
            "  order_status\n" +
            "FROM\n" +
            "\ttb_order o\n" +
            "LEFT JOIN tb_member m ON m.id = o.member_id\n" +
            "LEFT JOIN tb_setmeal s ON s.id = o.setmeal_id\n" +
            "WHERE\n" +
            "\to.id = #{id};")
    OrderDto findOrderDtoByOrderId(@Param("id")Long id);
}
