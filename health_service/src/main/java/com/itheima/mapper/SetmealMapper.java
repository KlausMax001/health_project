package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Setmeal;

public interface SetmealMapper extends BaseMapper<Setmeal> {

}
