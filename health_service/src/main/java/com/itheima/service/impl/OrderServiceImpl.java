package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.OrderDto;
import com.itheima.mapper.OrderMapper;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.MemberService;
import com.itheima.service.OrderService;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtil;
import com.itheima.utils.RedisUtil;
import com.itheima.vo.OrderVO;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/24 22:19
 */
@DubboService
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Resource
    private MemberService memberService;

    @Resource
    private OrderSettingService orderSettingService;

    @Override
    @Transactional
    public Long add(OrderVO orderVO) {
        //1.短信验证码校验
        String code = RedisUtil.get("sms:" + orderVO.getTelephone());
        if(ObjectUtils.isEmpty(code)){
            throw new RuntimeException("您未发送短信验证码或者短线验证码已失效！");
        }
        if(!code.equals(orderVO.getValidateCode())){
            throw new RuntimeException("验证码错误!");
        }
        String orderDate = DateUtil.formatDate(orderVO.getOrderDate());
        //2.判断是否重复预约
        Member member = memberService.findByTelephone(orderVO.getTelephone());
        if(member != null){
            Order order = exists(member.getId(),orderDate);
            if(order != null){
                throw new RuntimeException("请不要重复预约!");
            }
        }
        //3.当前日期是否可以预约
        OrderSetting orderSetting = orderSettingService.findByDate(orderDate);
        if(orderSetting == null){
            throw new RuntimeException("今日无预约!");
        }
        if(orderSetting.getNumber().intValue() == orderSetting.getReservations().intValue()){
            throw new RuntimeException("今日预约已满，不可预约！");
        }
        return doAdd(orderSetting,member,orderVO);
    }

    private Long doAdd(OrderSetting orderSetting,Member member,OrderVO orderVO){
        //4.预约设置预约数修改 +1
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingService.updateById(orderSetting);
        //5.会员信息维护
        if(member == null){
            member = new Member();
            member.setCreateDate(new Date());
        }
        BeanUtils.copyProperties(orderVO,member);
        memberService.saveOrUpdate(member);
        //6.预约订单
        Order order = new Order();
        order.setSetmealId(orderVO.getSetmealId());
        order.setMemberId(member.getId());
        order.setOrderDate(orderVO.getOrderDate());
        this.save(order);
        return order.getId();
    }

    @Override
    public Order exists(Long memberId, String orderDate) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Order::getMemberId,memberId)
                .eq(Order::getOrderDate,orderDate);
        return this.getOne(queryWrapper);
    }

    @Override
    public OrderDto findOrderDtoByOrderId(Long id) {

        return baseMapper.findOrderDtoByOrderId(id);
    }
}
