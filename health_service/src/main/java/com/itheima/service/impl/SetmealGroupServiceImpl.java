package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.service.SetmealGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.SetmealGroupMapper;
import com.itheima.pojo.SetmealGroup;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.Arrays;

@DubboService
public class SetmealGroupServiceImpl extends ServiceImpl<SetmealGroupMapper, SetmealGroup> implements SetmealGroupService {

    /**
     * 删除
     * @param id
     */
    public void deleteSetMealId(Long id) {
        QueryWrapper<SetmealGroup> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SetmealGroup::getSetmealId,id);
        remove(queryWrapper);
    }

    /**
     * 保存
     * @param id
     * @param groups
     */
    public void savaSetMealGroupId(Long id, Long[] groups) {

        if (groups != null && groups.length>0){
            ArrayList<SetmealGroup> setmealGroups = new ArrayList<>();

            Arrays.stream(groups).forEach(groupId->{
                SetmealGroup setmealGroup = new SetmealGroup();
                setmealGroup.setGroupId(groupId);
                setmealGroup.setSetmealId(id);
                setmealGroups.add(setmealGroup);
            });

           saveBatch(setmealGroups);
        }

    }


    @Override
    public Long[] selectGroupBySetMealId(Long setMealId) {
        return baseMapper.selectGroupBySetMealId(setMealId);
    }
}