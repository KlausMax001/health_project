package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.MemberMapper;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/24 19:33
 */
@DubboService
public class MemberServiceImpl extends ServiceImpl<MemberMapper,Member> implements MemberService {

    public Member findByTelephone(String telephone) {
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Member::getTelephone,telephone);
        return this.getOne(queryWrapper);
    }


    public Long saveMember(Member member) {
        return null;
    }
}
