package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.OrderSettingMapper;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtil;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DubboService
public class OrderSettingServiceImpl extends ServiceImpl<OrderSettingMapper, OrderSetting> implements OrderSettingService {


    @Override
    public void importData(List<String[]> list) {
        if (!list.isEmpty()) {
            List<OrderSetting> orderSettings = new ArrayList<>();
            list.stream().forEach(row -> {
                String orderDate = row[0];
                Integer num = Integer.valueOf(row[1]);
                //根据日期查询预约设置数据
                OrderSetting orderSetting = findByDate(orderDate);
                if(orderSetting == null){//如果不存在就新建数据
                    orderSetting = new OrderSetting();
                }
                orderSetting.setNumber(num);
                //字符串格式数据转化为日期对象
                orderSetting.setOrderDate(DateUtil.parseDate(orderDate));
                orderSettings.add(orderSetting);
            });
            //批量插入或更新数据
            this.saveOrUpdateBatch(orderSettings);
        }


    }


    public OrderSetting findByDate(String orderDate) {
        QueryWrapper<OrderSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(OrderSetting::getOrderDate,orderDate);

        return this.getOne(queryWrapper);
    }

    @Override
    public Map findOrderSettingByYearMonth(String yearMonth) {

        QueryWrapper<OrderSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().likeRight(OrderSetting::getOrderDate,yearMonth);

        List<OrderSetting> list = this.list(queryWrapper);
        HashMap map = new HashMap();
        list.stream().forEach(orderSetting -> {
            map.put(DateUtil.formatDate(orderSetting.getOrderDate()),orderSetting);

        });
        return map;
    }

    @Override
    public Boolean updateOrderSettingByData(String data, String num) {


        OrderSetting orderSetting = findByDate(data);
        orderSetting.setNumber(Integer.valueOf(num));

       return saveOrUpdate(orderSetting);


    }
}