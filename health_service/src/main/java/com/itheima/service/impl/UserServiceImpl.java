package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.MyUser;
import com.itheima.service.MyUserService;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/21 20:52
 */
@DubboService
public class UserServiceImpl extends ServiceImpl<UserMapper,MyUser> implements MyUserService {
}
