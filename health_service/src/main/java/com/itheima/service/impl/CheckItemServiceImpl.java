package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.CheckItemMapper;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/15 17:54
 */
@DubboService
public class CheckItemServiceImpl extends ServiceImpl<CheckItemMapper, CheckItem> implements CheckItemService {

    @Override
    public PageResult findByPage(QueryPageBean queryPageBean) {
        Page<CheckItem> page = this.page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        return new PageResult(page.getTotal(),page.getRecords());
    }

    @Override
    public PageResult queryByPage(QueryPageBean queryPageBean) {
        Page<CheckItem> page = this.page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        QueryWrapper<CheckItem> queryWrapper = new QueryWrapper<>();

        queryWrapper.lambda().like(CheckItem::getCode,queryPageBean.getQueryString()).or().like(CheckItem::getName,queryPageBean.getQueryString());

        List<CheckItem> list = this.list(queryWrapper);
        page.setRecords(list);
        page.setTotal(list.size());
        return new PageResult(page.getTotal(),page.getRecords());
    }

    @Override
    public List<CheckItem> findByGroupId(Long id) {
        return baseMapper.findByGroupId(id);
    }

}
