package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.CheckGroupItemMapper;
import com.itheima.pojo.CheckGroupItem;
import com.itheima.service.CheckGroupItemService;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:45
 */
@DubboService
public class CheckGroupItemServiceImpl extends ServiceImpl<CheckGroupItemMapper, CheckGroupItem> implements CheckGroupItemService {
    /**
     * 清空tb_check_group_item中指定检查组id的所有信息
     *
     * @param id
     */
    @Override
    public void deleteByGroupId(Long id) {
        QueryWrapper<CheckGroupItem> checkGroupItemQueryWrapper = new QueryWrapper<>();
        checkGroupItemQueryWrapper.lambda().eq(CheckGroupItem::getGroupId, id);
        remove(checkGroupItemQueryWrapper);
    }

    /**
     * 批量插入维护中间表
     *
     * @param id
     * @param items
     */
    @Override
    public void saveCheckGroupItem(Long id, Long[] items) {
        if(items != null && items.length > 0){
            List<CheckGroupItem> list = new ArrayList<>();
            Arrays.stream(items).forEach(itemId -> {
                CheckGroupItem checkGroupItem = new CheckGroupItem();
                checkGroupItem.setGroupId(id);
                checkGroupItem.setItemId(itemId);
                list.add(checkGroupItem);
            });
            this.saveBatch(list);
        }
    }

    @Override
    public Long[] selectItemsByGroupId(Long groupId) {
        return baseMapper.selectItemsByGroupId(groupId);
    }


}
