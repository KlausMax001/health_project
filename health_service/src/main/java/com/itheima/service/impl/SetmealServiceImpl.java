package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.SetmealDto;
import com.itheima.mapper.SetmealMapper;
import com.itheima.pojo.Setmeal;
import com.itheima.service.CheckGroupService;
import com.itheima.service.CheckItemService;
import com.itheima.service.SetmealService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.utils.FileUtil;
import com.itheima.utils.RedisUtil;
import com.itheima.vo.SetMealVO;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Set;
import java.util.stream.Collectors;

@DubboService
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Resource
    private SetmealGroupServiceImpl setmealGroupServiceImpl;


    @Resource
    private CheckGroupService checkGroupService;

    @Resource
    private CheckItemService checkItemService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 分页查找
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findByPage(QueryPageBean queryPageBean) {
        IPage<Setmeal> iPage = this.page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        return new PageResult(iPage.getTotal(), iPage.getRecords());
    }

    /**
     * 保存下单套餐信息
     *
     * @return
     */
    @Override
    @Transactional
    public boolean saveSetMeal(SetMealVO setMealVO) {
        //将套餐信息保存到套餐表中
        saveOrUpdate(setMealVO);
        //将最新的图片文件，从temp-rubbish中移除
        if (!ObjectUtils.isEmpty(setMealVO.getImg())) {
//            redisTemplate.opsForSet().remove("temp-rubbish", setMealVO.getImg());
            RedisUtil.removeOfSet("temp-rubbish", setMealVO.getImg());
        }

        //将删除原有套餐和检查组的关系
        setmealGroupServiceImpl.deleteSetMealId(setMealVO.getId());

        //插入新维护的套餐和检查组关系到中间表
        setmealGroupServiceImpl.savaSetMealGroupId(setMealVO.getId(), setMealVO.getGroups());
        return true;
    }

    /**
     * 清理oss图片文件垃圾的方法
     */
    @Override
    public void clearOssPic() {
        Set<String> rubbishSet = redisTemplate.opsForSet()
                .members("temp-rubbish")
                .stream()
                .map(fileKeyObj -> (String) fileKeyObj)
                .collect(Collectors.toSet());

        if(!rubbishSet.isEmpty()){
            rubbishSet.forEach(fileKey -> {
                //判断辅助key是否存在(文件是否过期)
                if(!redisTemplate.hasKey("file:" + fileKey)){
                    //删除oss文件
                    FileUtil.delete(fileKey);
//                    redisTemplate.opsForSet().remove("temp-rubbish",fileKey);
                    RedisUtil.removeOfSet("temp-rubbish",fileKey);
                }
            });
        }
    }

    @Override
    public SetmealDto detail(Long setmealId) {

        Setmeal setmeal = this.getById(setmealId);

        if(setmeal != null){
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(setmeal,setmealDto);

            //2.通过套餐查询检查组
            setmealDto.setCheckGroups(checkGroupService.selectBySetmeal(setmealId));
            //3.维护检查组对应的检查项列表
            if(setmealDto.getCheckGroups() != null){
                setmealDto.getCheckGroups().stream().forEach(checkGroupDTO -> {
                    checkGroupDTO.setCheckItems(checkItemService.findByGroupId(checkGroupDTO.getId()));
                });
            }

            return setmealDto;
        }
        return null;
    }
}