package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.CheckGroupDto;
import com.itheima.mapper.CheckGroupMapper;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupItemService;
import com.itheima.service.CheckGroupService;
import com.itheima.support.PageResult;
import com.itheima.support.QueryPageBean;
import com.itheima.vo.CheckGroupVO;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/16 18:46
 */
@DubboService
public class CheckGroupServiceImpl extends ServiceImpl<CheckGroupMapper, CheckGroup> implements CheckGroupService {

    @Resource
    private CheckGroupItemService checkGroupItemService;


    /**
     * 保存新建检查组
     * 开启事务
     * @param checkGroupVO
     * @return
     */

    @Transactional
    public boolean saveGroup(CheckGroupVO checkGroupVO) {
        //1.保存检查组基本信息 向tb_check_group中插入
        this.saveOrUpdate(checkGroupVO);

        //2.先根据检查组ID删除原有的检查组与检查项的关系
        checkGroupItemService.deleteByGroupId(checkGroupVO.getId());

        //3.插入新维护的检查组与检查项关系
        checkGroupItemService.saveCheckGroupItem(checkGroupVO.getId(),checkGroupVO.getItems());

        return true;
    }

    /**
     *分页查询所有检查组信息
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findAllByPage(QueryPageBean queryPageBean) {
        IPage<CheckGroup> iPage =this.page(new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize()));

        return new PageResult(iPage.getTotal(),iPage.getRecords());
    }

    @Override
    public List<CheckGroupDto> selectBySetmeal(Long setmealId) {
        return baseMapper.selectBySetmeal(setmealId);
    }

}
