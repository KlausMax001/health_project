package com.itheima.job;

import com.itheima.service.SetmealService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/19 21:01
 */
@Component
//保证业务唯一
@JobHandler("clear.oss.job")
public class ClearOssPicJob extends IJobHandler {
    @Resource
    public SetmealService setmealService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        System.out.println("clear oss job。。。 " );
        setmealService.clearOssPic();
        return SUCCESS;
    }
}
