package com.itheima.listener;

import com.itheima.cst.SysConsts;
import com.itheima.service.impl.UserServiceImpl;
import com.itheima.utils.RedisUtil;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Runtime.getRuntime;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/21 20:43
 */
@Component
public class MyStartuplistener implements BaseApplicationListener {

    @Resource
    private UserServiceImpl userService;

    //调用多线程异步加载执行服务
    private ExecutorService executorService = Executors.newFixedThreadPool(getRuntime().availableProcessors() * 2);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        executorService.execute(() -> {
            //将用户表信息加载到缓存中
            userService.list().stream().forEach(myUser -> {
                RedisUtil.set(SysConsts.REDIS_USER_PREFIX + myUser.getUserName(), myUser);
            });
        });


    }
}
