package com.itheima.listener;

import org.springframework.context.event.ContextRefreshedEvent;

public interface BaseApplicationListener {

  void onApplicationEvent(ContextRefreshedEvent event);

}
