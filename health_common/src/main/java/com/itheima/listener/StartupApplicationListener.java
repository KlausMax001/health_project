package com.itheima.listener;

import com.itheima.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired(required = false)
    private List<BaseApplicationListener> listeners;
    /**
     * 在监听启动类事件中注册redis并完成其编码序列化
     * @param event
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        RedisUtil.register(redisTemplate);

        //执行其它项目的监听器
        if(listeners != null){
            listeners.stream().forEach(listener -> {
                listener.onApplicationEvent(event);
            });
        }
    }

}
