package com.itheima.utils;

import java.security.*;

public class RsaUtil {

    private static final String secret = "c1h2u3a4n5z6h7i8j9i1a1n2k3a4n5g";

    private static KeyPair keyPair;

    static {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            SecureRandom secureRandom = new SecureRandom(secret.getBytes());
            keyPairGenerator.initialize(1024, secureRandom);
            keyPair = keyPairGenerator.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static PublicKey getPublicKey(){
        return keyPair.getPublic();
    }
    public static PrivateKey getPrivateKey(){
        return keyPair.getPrivate();
    }

}
