package com.itheima.utils;


import org.apache.commons.codec.digest.Md5Crypt;

public class MD5Util {


	private static String salt = "$1$2$3";

	public static String encode(String data){
		return Md5Crypt.md5Crypt(data.getBytes(),salt);
	}

//	public static void main(String[] args) {
//		System.out.println(MD5Util.encode("123"));
//		System.out.println(MD5Util.("456"));
//	}

}
