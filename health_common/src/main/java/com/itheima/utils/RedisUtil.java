package com.itheima.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.concurrent.TimeUnit;


public class RedisUtil {

    private static RedisTemplate<String, Object> redisTemplate;

    private static volatile boolean inited;

    public static void register(RedisTemplate<String, Object> _redisTemplate){
        if(inited == false){
            redisTemplate = _redisTemplate;
            redisTemplate.setKeySerializer(new StringRedisSerializer());
            redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
            inited = true;
        }
    }

    public static void set(String key,Object value){
        redisTemplate.opsForValue().set(key,value);
    }

    public static void set(String key,Object value,long timeout, TimeUnit unit){
        redisTemplate.opsForValue().set(key,value,timeout,unit);
    }

    public static <T> T get(String key){
        return (T) redisTemplate.opsForValue().get(key);
    }

    public static void removeOfSet(String key, Object... values){
        redisTemplate.opsForSet().remove(key,values);
    }

    public static void addToSet(String key, Object... values){
        redisTemplate.opsForSet().add(key,values);
    }

}
