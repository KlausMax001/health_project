package com.itheima.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;

import java.io.InputStream;
import java.util.UUID;

public class FileUtil {

    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    public static final String ENDPOINT = "oss-cn-shanghai.aliyuncs.com";

    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    public static final String ACCESS_KEY_ID = "LTAI5t7LB1SNAWFiadZng2Zw";
    public static final String ACCESS_KEY_SECRET = "zZPKsL2eBLtD8Ul598gZ2933PNQUZq";

    // String bucketName, String key, InputStream input
    // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
    public static final String BUCKET_NAME = "klaus-122";

    public static String upload(InputStream inputStream,String filename){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。

        String key = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        ossClient.putObject(BUCKET_NAME, key, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
        return key;
    }

    public static boolean delete(String fileKey){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。

        ossClient.deleteObject(BUCKET_NAME,fileKey);

        // 关闭OSSClient。
        ossClient.shutdown();
        return true;
    }

}
